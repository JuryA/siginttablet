#!/bin/bash

cd ~/source

echo "Installing Subaru Keyfob exploit"

if [ ! -d ~/source/bluepy ]; then
    git clone https://github.com/tomwimmenhove/subarufobrob.git
    cd subarufobrob
else
    cd subarufobrob
    git pull
fi
mkdir build
cd build
cmake ..
make

cd ~/source
