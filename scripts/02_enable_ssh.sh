#!/bin/bash

# Enable and start SSH
sudo systemctl enable ssh
sudo systemctl start ssh

if grep -q ServerAliveInterval /etc/ssh/sshd_config; then
    # Look for ServerAliveInterval
    if grep -q "ServerAliveInterval 60" /etc/ssh/sshd_config; then
         # We found ServerAliveInterval 60, so we're cool.
         echo "ServerAliveInterval already set. No-op"
    else
         # We found ServerAliveInterval without 60 so add it
         sed -i 's/ServerAliveInterval.*/& 60 /g' /etc/ssh/sshd_config
         echo "Adding 60 to the existing ServerAliveInterval line"
    fi
else
    # We didn't find ServerAliveInterval , so add the line
    echo "ServerAliveInterval 60" >> /etc/ssh/sshd_config
    echo "Adding ServerAliveInterval line"
fi
